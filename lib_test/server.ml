open Lwt

let network : (module Net_overlay.Sig.S) = (module Lwt_unix)

module Network = (val network)

(* input from fdin to network *)
let input_from fdin fdout =
  let buffer_size = 4096 in
  let buffer = Bytes.create buffer_size in
  let rec copy () =
    Lwt_unix.read fdin buffer 0 buffer_size >>= fun res ->
    match res with
    | 0 -> Lwt.return ()
    | n -> Network.write fdout buffer 0 n >>= fun _res -> copy ()
  in
  copy ()

(* output from network to fdout *)
let output_to fdin fdout =
  let buffer_size = 4096 in
  let buffer = Bytes.create buffer_size in
  let rec copy () =
    Network.read fdin buffer 0 buffer_size >>= fun res ->
    match res with
    | 0 -> Lwt.return ()
    | n -> Lwt_unix.write fdout buffer 0 n >>= fun _res -> copy ()
  in
  copy ()

let server () =
  let port = int_of_string Sys.argv.(1) in
  let host = (Unix.gethostbyname (Unix.gethostname ())).h_addr_list.(0) in
  let addr = Unix.ADDR_INET (host, port) in
  let listening = Network.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  Network.bind listening addr >>= fun () ->
  Network.listen listening 1 ;
  let buffer_size = 4096 in
  let buffer = Bytes.create buffer_size in
  let rec loop () =
    Network.accept listening >>= fun (comm, _addr) ->
    let rec copy acc =
      Network.read comm buffer 0 buffer_size >>= fun res ->
      match res with
      | 0 -> Lwt.return acc
      | n ->
          let sub = Bytes.sub_string buffer 0 n in
          copy (acc ^ sub)
    in
    copy "" >>= fun result ->
    Format.printf "client sent %s@." result ;
    loop ()
  in
  loop ()

let () = Lwt_main.run @@ server ()
