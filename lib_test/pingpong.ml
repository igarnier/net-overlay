module Net = Net_overlay.Local.Init ()

module Server_network = Net.Make (struct
  let hostname = "server.com"

  let address = Unix.inet_addr_of_string "253.86.12.98"

  let buffer_size = 1024
end)

module Client_network = Net.Make (struct
  let hostname = "client.com"

  let address = Unix.inet_addr_of_string "223.16.52.48"

  let buffer_size = 1024
end)

let server_network : (module Net_overlay.Sig.S) = (module Server_network)

let client_network : (module Net_overlay.Sig.S) = (module Client_network)

open Lwt

let read_exactly comm buffer n =
  let rec read_loop rem acc =
    if rem = 0 then Lwt.return (Some acc)
    else
      Server_network.read comm buffer 0 rem >>= fun res ->
      match res with
      | 0 -> Lwt.return None
      | n ->
          if n > rem then assert false
          else
            let sub = Bytes.sub_string buffer 0 n in
            read_loop (rem - n) (acc ^ sub)
  in
  read_loop n ""

let server () =
  Server_network.gethostname () >>= fun host ->
  Server_network.gethostbyname host >>= fun { Unix.h_addr_list; _ } ->
  let host = h_addr_list.(0) in
  Format.printf "server addr: %s@." (Unix.string_of_inet_addr host) ;
  let addr = Unix.ADDR_INET (host, 66) in
  let listening = Server_network.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  Server_network.bind listening addr >>= fun () ->
  Server_network.listen listening 1 ;
  let buffer_size = 4096 in
  let buffer = Bytes.create buffer_size in
  Server_network.accept listening >>= fun (comm, _addr) ->
  let rec loop () =
    read_exactly comm buffer 4 >>= function
    | None ->
        Format.printf "server exiting@." ;
        exit 0
    | Some ping ->
        Format.printf "server received: %s@." ping ;
        Server_network.write comm (Bytes.of_string "pong") 0 4
        >>= fun nwritten ->
        assert (nwritten = 4) ;
        loop ()
  in
  loop ()

let read_exactly comm buffer n =
  let rec read_loop rem acc =
    if rem = 0 then Lwt.return (Some acc)
    else
      Client_network.read comm buffer 0 rem >>= fun res ->
      match res with
      | 0 -> Lwt.return None
      | n ->
          if n > rem then assert false
          else
            let sub = Bytes.sub_string buffer 0 n in
            read_loop (rem - n) (acc ^ sub)
  in
  read_loop n ""

let client () =
  Client_network.gethostbyname "server.com" >>= fun info ->
  let addr = info.h_addr_list.(0) in
  Format.printf "server addr: %s@." (Unix.string_of_inet_addr addr) ;
  let sock = Client_network.socket PF_INET SOCK_STREAM 0 in
  Lwt_unix.sleep 1.0 >>= fun () ->
  Client_network.connect sock (ADDR_INET (addr, 66)) >>= fun () ->
  let buffer_size = 4096 in
  let buffer = Bytes.create buffer_size in
  let rec loop () =
    Client_network.write sock (Bytes.of_string "ping") 0 4 >>= fun nwritten ->
    assert (nwritten = 4) ;
    read_exactly sock buffer 4 >>= function
    | None -> exit 0
    | Some pong ->
        Format.printf "client received: %s@." pong ;
        loop ()
  in
  loop ()

let threads = Lwt_main.run (Lwt.join [server (); client ()])
