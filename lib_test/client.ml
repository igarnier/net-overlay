(* Code adapted from the OCaml unix system programming book
   https://ocaml.github.io/ocamlunix/sockets.html *)

open Lwt

let network : (module Net_overlay.Sig.S) = (module Lwt_unix)

module Network = (val network)

let retransmit fdin fdout =
  let buffer_size = 4096 in
  let buffer = Bytes.create buffer_size in
  let rec copy () =
    Network.read fdin buffer 0 buffer_size >>= fun res ->
    match res with
    | 0 -> Lwt.return ()
    | n -> Network.write fdout buffer 0 n >>= fun _res -> copy ()
  in
  copy ()

let input_from fdin fdout =
  let buffer_size = 4096 in
  let buffer = Bytes.create buffer_size in
  let rec copy () =
    Lwt_unix.read fdin buffer 0 buffer_size >>= fun res ->
    match res with
    | 0 -> Lwt.return ()
    | n -> Network.write fdout buffer 0 n >>= fun _res -> copy ()
  in
  copy ()

let client () =
  if Array.length Sys.argv < 3 then (
    prerr_endline "Usage: client <host> <port>" ;
    exit 2 ) ;
  let server_name = Sys.argv.(1) and port_number = int_of_string Sys.argv.(2) in
  let server_addr =
    try (Unix.gethostbyname server_name).h_addr_list.(0)
    with Not_found ->
      prerr_endline (server_name ^ ": Host not found") ;
      exit 2
  in
  let sock = Network.socket PF_INET SOCK_STREAM 0 in
  Network.connect sock (ADDR_INET (server_addr, port_number)) >>= fun () ->
  Lwt.join [input_from Lwt_unix.stdin sock]

let () = Lwt_main.run @@ client ()
