# net-overlay

## What?

This library implements a small subset of the TCP/UDP stack in userland.
Concretely, this implements a small subset of the interface provided by the
`Lwt_unix` module of the `Lwt` library.

## Why!?

This allows to implement in a concurrent but sequential fashion (hence more easily
debuggable) algorithms such as p2p libraries, etc.

Distributed algorithms implementations generic over the interface provided by
net-overlay (eg using functors or 1st class modules) can be seamlessly executed
over the internet (using the `Lwt_unix` implementation) or locally
(using `net-overlay`).

## Ah.

This is still a work in progress and a proof of concept. The implementation
respects the types but clearly not the TCP/IP standard (eg some flags are
ignored, some errors are probably not handled the right way, etc).
