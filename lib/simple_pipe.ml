open Lwt

type chunk = Bytes.t

type state = Open | Closed_by_writer | Closed_by_reader | Really_closed

type t =
  { queue : Bytes.t Queue.t;
    (* chunk, offset in chunk *)
    mutable current_chunk : (Bytes.t * int) option;
    wakeup_reader : unit Lwt_condition.t;
    wakeup_writer : unit Lwt_condition.t;
    mutex : Lwt_mutex.t;
    mutable size : int;
    max_size : int;
    mutable state : state
  }

let create max_size =
  { queue = Queue.create ();
    current_chunk = None;
    wakeup_reader = Lwt_condition.create ();
    wakeup_writer = Lwt_condition.create ();
    mutex = Lwt_mutex.create ();
    size = 0;
    max_size;
    state = Open
  }

let writer_close pipe =
  Lwt_condition.signal pipe.wakeup_writer () ;
  Lwt_condition.signal pipe.wakeup_reader () ;
  match pipe.state with
  | Open ->
      pipe.state <- Closed_by_writer ;
      Lwt.return_unit
  | Closed_by_reader ->
      pipe.state <- Really_closed ;
      pipe.current_chunk <- None ;
      pipe.size <- -1 ;
      Lwt.return_unit
  | Really_closed | Closed_by_writer ->
      Lwt.fail Unix.(Unix_error (EBADF, "writer_close", ""))

let reader_close pipe =
  Lwt_condition.signal pipe.wakeup_writer () ;
  Lwt_condition.signal pipe.wakeup_reader () ;
  match pipe.state with
  | Open ->
      pipe.state <- Closed_by_reader ;
      Lwt.return_unit
  | Closed_by_reader | Really_closed ->
      Lwt.fail Unix.(Unix_error (EBADF, "reader_close", ""))
  | Closed_by_writer ->
      pipe.state <- Really_closed ;
      pipe.current_chunk <- None ;
      pipe.size <- -1 ;
      Lwt.return_unit

(* Can only be called when pipe is locked. *)
let perform_write pipe bytes ofs len =
  match pipe.state with
  | Closed_by_reader -> Lwt.fail Unix.(Unix_error (EPIPE, "perform_write", ""))
  | Really_closed | Closed_by_writer ->
      Lwt.fail Unix.(Unix_error (EBADF, "perform_write", ""))
  | Open ->
      let remaining = pipe.max_size - pipe.size in
      assert (remaining > 0) ;
      let writable = min remaining len in
      let chunk = Bytes.sub bytes ofs writable in
      Queue.push chunk pipe.queue ;
      Lwt_condition.signal pipe.wakeup_reader () ;
      Lwt.return writable

(* Blocks when pipe is full. *)
let write (pipe : t) (bytes : Bytes.t) (ofs : int) (len : int) =
  match pipe.state with
  | Closed_by_reader -> Lwt.fail Unix.(Unix_error (EPIPE, "write", ""))
  | Really_closed | Closed_by_writer ->
      Lwt.fail Unix.(Unix_error (EBADF, "write", ""))
  | Open ->
      Lwt_mutex.with_lock pipe.mutex (fun () ->
          let remaining = pipe.max_size - pipe.size in
          assert (remaining >= 0) ;
          if remaining = 0 then
            Lwt_condition.wait ~mutex:pipe.mutex pipe.wakeup_writer
            >>= fun () -> perform_write pipe bytes ofs len
          else perform_write pipe bytes ofs len)

(* This function must be called when the lock pipe.mutex is taken. *)
let rec get_current_chunk (pipe : t) =
  match (Queue.peek_opt pipe.queue, pipe.state) with
  | (None, (Really_closed | Closed_by_reader)) ->
      Lwt.fail Unix.(Unix_error (EBADF, "get_current_chunk", ""))
  | (None, Closed_by_writer) -> Lwt.return None
  | (None, Open) ->
      (* Take a chunk from the queue *)
      Lwt_condition.wait ~mutex:pipe.mutex pipe.wakeup_reader >>= fun () ->
      get_current_chunk pipe
  | (Some _, _) ->
      let result = (Queue.pop pipe.queue, 0) in
      pipe.current_chunk <- Some result ;
      Lwt.return pipe.current_chunk

(* Reads only one chunk; we could imagine reading several chunks
   when possible. *)
let read (pipe : t) (dest : Bytes.t) (ofs : int) (len : int) =
  match pipe.state with
  | Really_closed | Closed_by_reader ->
      Lwt.fail Unix.(Unix_error (EBADF, "read", ""))
  | Closed_by_writer | Open ->
      if len <= 0 then Lwt.fail_with "Simple_pipe.read: len <= 0"
      else
        Lwt_mutex.with_lock pipe.mutex (fun () ->
            ( match pipe.current_chunk with
            | None -> get_current_chunk pipe
            | Some _ -> Lwt.return pipe.current_chunk )
            >>= function
            | None -> Lwt.return 0
            | Some (chunk, offset) ->
                let remaining = Bytes.length chunk - offset in
                assert (remaining > 0) ;
                if remaining <= len then (
                  pipe.current_chunk <- None ;
                  Bytes.blit chunk offset dest ofs remaining ;
                  Lwt_condition.signal pipe.wakeup_writer () ;
                  Lwt.return remaining )
                else (
                  Bytes.blit chunk offset dest ofs len ;
                  let offset = offset + len in
                  pipe.current_chunk <- Some (chunk, offset) ;
                  Lwt_condition.signal pipe.wakeup_writer () ;
                  Lwt.return len ))
