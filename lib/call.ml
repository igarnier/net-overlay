[@@@ocaml.warning "-32"]

let socket (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.socket

let setsockopt (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.setsockopt

let bind (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.bind

let listen (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.listen

let accept (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.accept

let connect (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.connect

let read (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.read

let write (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.write

let recvfrom (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.recvfrom

let recvfrom (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.recvfrom

let sendto (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.sendto

let sleep (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.sleep

let now (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.now

let close (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.close

let gethostname (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.gethostname

let gethostbyname (type fd) (module Overlay : Sig.S with type file_descr = fd) =
  Overlay.gethostbyname
