module type S = sig
  type file_descr

  (** Socket creation. *)
  val socket : Unix.socket_domain -> Unix.socket_type -> int -> file_descr

  (** Setting socket options. *)
  val setsockopt : file_descr -> Unix.socket_bool_option -> bool -> unit

  val bind : file_descr -> Unix.sockaddr -> unit Lwt.t

  val listen : file_descr -> int -> unit

  val accept : file_descr -> (file_descr * Unix.sockaddr) Lwt.t

  val connect : file_descr -> Unix.sockaddr -> unit Lwt.t

  val read : file_descr -> bytes -> int -> int -> int Lwt.t

  val write : file_descr -> bytes -> int -> int -> int Lwt.t

  val recvfrom :
    file_descr ->
    bytes ->
    int ->
    int ->
    Unix.msg_flag list ->
    (int * Unix.sockaddr) Lwt.t

  val sendto :
    file_descr ->
    bytes ->
    int ->
    (* ofs *)
    int ->
    (* len *)
    Unix.msg_flag list ->
    Unix.sockaddr ->
    int Lwt.t

  val sleep : float -> unit Lwt.t

  val now : unit -> float

  val close : file_descr -> unit Lwt.t

  val gethostname : unit -> string Lwt.t

  val gethostbyname : string -> Unix.host_entry Lwt.t
end

type 'fd t = (module S with type file_descr = 'fd)
