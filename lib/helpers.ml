(* Pretty-printing Unix constants *)
let pp_socket_domain : Format.formatter -> Unix.socket_domain -> unit =
 fun fmtr dom ->
  match dom with
  | Unix.PF_UNIX -> Format.fprintf fmtr "PF_UNIX"
  | Unix.PF_INET -> Format.fprintf fmtr "PF_INET"
  | Unix.PF_INET6 -> Format.fprintf fmtr "PF_INET6"

let pp_socket_type : Format.formatter -> Unix.socket_type -> unit =
 fun fmtr typ ->
  match typ with
  | Unix.SOCK_STREAM -> Format.fprintf fmtr "SOCK_STREAM"
  | Unix.SOCK_DGRAM -> Format.fprintf fmtr "SOCK_DGRAM"
  | Unix.SOCK_RAW -> Format.fprintf fmtr "SOCK_RAW"
  | Unix.SOCK_SEQPACKET -> Format.fprintf fmtr "SOCK_SEQPACKET"

let pp_inet_addr : Format.formatter -> Unix.inet_addr -> unit =
 fun fmtr addr -> Format.fprintf fmtr "%s" (Unix.string_of_inet_addr addr)

let pp_socket_bool_option : Format.formatter -> Unix.socket_bool_option -> unit
    =
 fun fmtr opt ->
  match opt with
  | Unix.SO_DEBUG -> Format.fprintf fmtr "SO_DEBUG"
  | Unix.SO_BROADCAST -> Format.fprintf fmtr "SO_BROADCAST"
  | Unix.SO_REUSEADDR -> Format.fprintf fmtr "SO_REUSEADDR"
  | Unix.SO_KEEPALIVE -> Format.fprintf fmtr "SO_KEEPALIVE"
  | Unix.SO_DONTROUTE -> Format.fprintf fmtr "SO_DONTROUTE"
  | Unix.SO_OOBINLINE -> Format.fprintf fmtr "SO_OOBINLINE"
  | Unix.SO_ACCEPTCONN -> Format.fprintf fmtr "SO_ACCEPTCONN"
  | Unix.TCP_NODELAY -> Format.fprintf fmtr "TCP_NODELAY"
  | Unix.IPV6_ONLY -> Format.fprintf fmtr "IPV6_ONLY"

let pp_sockaddr fmtr (a : Unix.sockaddr) =
  match a with
  | Unix.ADDR_UNIX s -> Format.fprintf fmtr "ADDR_UNIX(%s)" s
  | Unix.ADDR_INET (inet, port) ->
      Format.fprintf
        fmtr
        "ADDR_INET(%s,%d)"
        (Unix.string_of_inet_addr inet)
        port

(* Comparisong function *)
let sockadr_compare (a : Unix.sockaddr) (b : Unix.sockaddr) =
  match (a, b) with
  | (Unix.ADDR_UNIX sa, Unix.ADDR_UNIX sb) -> String.compare sa sb
  | (Unix.ADDR_UNIX _, _) -> -1
  | (Unix.ADDR_INET _, Unix.ADDR_UNIX _) -> 1
  | (Unix.ADDR_INET (addr1, port1), Unix.ADDR_INET (addr2, port2)) ->
      let c = Stdlib.compare addr1 addr2 in
      if c <> 0 then c else Int.compare port1 port2
