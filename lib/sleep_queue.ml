module Key = struct
  type t = { resolver : unit Lwt.u; key : int }

  let compare x y = Int.compare x.key y.key

  let assign_id =
    let x = ref 0 in
    fun resolver ->
      let key = !x in
      incr x ;
      { resolver; key }
end

module Prio = Float
module Prioqueue = Psq.Make (Key) (Prio)

module Create () = struct
  type t = { mutable now : float; mutable queue : Prioqueue.t }

  let now = ref 0.0

  let queue = ref Prioqueue.empty

  let rec scheduler queue =
    match Prioqueue.pop queue with
    | None -> queue
    | Some ((key, wakeup_time), rest) ->
        if wakeup_time <= !now then (
          Lwt.wakeup_later key.resolver () ;
          scheduler rest )
        else queue

  let elapse seconds =
    assert (seconds > 0.) ;
    now := !now +. seconds ;
    queue := scheduler !queue

  let sleep seconds =
    let (promise, resolver) = Lwt.task () in
    let wakeup_at = !now +. seconds in
    queue := Prioqueue.add (Key.assign_id resolver) wakeup_at !queue ;
    promise

  let now () = !now
end
