open Lwt
open Helpers

module Sockaddr_map = Map.Make (struct
  type t = Unix.sockaddr

  let compare = Helpers.sockadr_compare
end)

(* We could avoir some useless pattern matchings by
   using GADT for the socket_state. *)

type file_descr =
  { uid : int;
    dom : Unix.socket_domain;
    kind : Unix.socket_type;
    mutable addr : Unix.sockaddr option;
    mutable state : socket_state
  }

and socket_state =
  | Datagram_state of datagram_socket
  | Stream_state of stream_socket

and datagram_socket =
  { mutable datagram_state : datagram_state;
    mutable datagrams : Bytes.t Queue.t Sockaddr_map.t;
    datagram_lock : Lwt_mutex.t;
    datagram_cond : unit Lwt_condition.t
  }

and stream_socket =
  { mutable stream_state : stream_state;
    conns_lock : Lwt_mutex.t;
    conns_cond : unit Lwt_condition.t
  }

and datagram_state =
  | Datagram_neutral
  | Datagram_bound
  | Datagram_connected
  | Datagram_closed

and stream_state =
  | Stream_neutral
  | Stream_bound
  | Stream_listening of listening_state
  | Stream_connecting of channels option Lwt_condition.t
  | Stream_connected of channels
  | Stream_closed

and listening_state =
  { max_connections : int; mutable pending : pending_connection list }

and pending_connection = { client_socket : file_descr }

(* points are socket uids *)
and channels =
  { points : int * int; read_from : Simple_pipe.t; write_to : Simple_pipe.t }

let _ = Datagram_connected

let pp_datagram_state fmtr (dstate : datagram_state) =
  let open Format in
  match dstate with
  | Datagram_neutral -> pp_print_string fmtr "Datagram_neutral"
  | Datagram_bound -> pp_print_string fmtr "Datagram_bound"
  | Datagram_connected -> pp_print_string fmtr "Datagram_connected"
  | Datagram_closed -> pp_print_string fmtr "Datagram_closed"

let pp_stream_state fmtr (sstate : stream_state) =
  let open Format in
  match sstate with
  | Stream_neutral -> pp_print_string fmtr "Stream_neutral"
  | Stream_bound -> pp_print_string fmtr "Stream_bound"
  | Stream_listening _ -> pp_print_string fmtr "Stream_listening"
  | Stream_connecting _ -> pp_print_string fmtr "Stream_connecting"
  | Stream_connected _ -> pp_print_string fmtr "Stream_connected"
  | Stream_closed -> pp_print_string fmtr "Stream_closed"

let pp_file_descr fmtr fd =
  Format.fprintf
    fmtr
    "{ uid = %d ; addr = %a ; state = %a }"
    fd.uid
    (fun fmtr addr_opt ->
      match addr_opt with
      | None -> Format.fprintf fmtr "None"
      | Some addr -> Helpers.pp_sockaddr fmtr addr)
    fd.addr
    (fun fmtr (state : socket_state) ->
      match state with
      | Datagram_state { datagram_state; _ } ->
          pp_datagram_state fmtr datagram_state
      | Stream_state { stream_state; _ } -> pp_stream_state fmtr stream_state)
    fd.state

let _is_closed : file_descr -> bool =
 fun fd ->
  match fd.state with
  | Datagram_state { datagram_state; _ } -> (
      match datagram_state with
      | Datagram_neutral | Datagram_bound | Datagram_connected -> false
      | Datagram_closed -> true )
  | Stream_state { stream_state; _ } -> (
      match stream_state with
      | Stream_neutral | Stream_bound | Stream_listening _ | Stream_connecting _
      | Stream_connected _ ->
          false
      | Stream_closed -> true )

let _is_bound : file_descr -> bool =
 fun fd ->
  match fd.state with
  | Datagram_state { datagram_state; _ } -> (
      match datagram_state with
      | Datagram_neutral | Datagram_connected | Datagram_closed -> false
      | Datagram_bound -> true )
  | Stream_state { stream_state; _ } -> (
      match stream_state with
      | Stream_neutral | Stream_listening _ | Stream_connecting _
      | Stream_connected _ | Stream_closed ->
          false
      | Stream_bound -> true )

let _is_neutral : file_descr -> bool =
 fun fd ->
  match fd.state with
  | Datagram_state { datagram_state; _ } -> (
      match datagram_state with
      | Datagram_connected | Datagram_closed | Datagram_bound -> false
      | Datagram_neutral -> true )
  | Stream_state { stream_state; _ } -> (
      match stream_state with
      | Stream_listening _ | Stream_connecting _ | Stream_connected _
      | Stream_closed | Stream_bound ->
          false
      | Stream_neutral -> true )

module type Local_parameters = sig
  val hostname : string

  val address : Unix.inet_addr

  val buffer_size : int
end

(* Maps addresses to sockets. Not all sockets have addresses. There may be many
   sockets associated to the same address. *)
module Socket_table = Hashtbl.Make (struct
  type t = Unix.sockaddr

  (* /!\ polymorphic hash (because inet_addr is abstract). We know inet_addr is
     a string in the current implem so it's ok. *)
  let hash = Hashtbl.hash

  (* /!\ polymorphic equality (see above) *)
  let equal = Stdlib.( = )
end)

(* Map from domain names to [Unix.host_entry] *)
module Dns = Hashtbl.Make (struct
  type t = String.t

  let hash = Hashtbl.hash

  let equal = String.equal
end)

(* Hash set of (SOCK_STREAM) links *)
module Links = Hashtbl.Make (struct
  type t = int * int

  let hash (x, y) = if x <= y then Hashtbl.hash (x, y) else Hashtbl.hash (y, x)

  let equal (x, y) (x', y') = Int.equal x x' && Int.equal y y'
end)

(* We also need to generate ports for clients connecting without
   binding first. We make those globally unique for simplicity, though
   technically they should be unique per-host. *)
let gen_port =
  let x = ref 0 in
  fun () ->
    let v = !x in
    incr x ;
    v

module Init_common (Time_impl : sig
  val sleep : float -> unit Lwt.t

  val now : unit -> float
end) =
struct
  let table_size = 503 (* arbitrary prime cst *)

  (* subset of stream sockets that are bound or listening *)
  let bound_stream_table = Socket_table.create table_size

  (* this contains all stream sockets that have an address *)
  let stream_table = Socket_table.create table_size

  (* this contains all datagram sockets that have an address
     (and which are thus reachable) *)
  let datagram_table = Socket_table.create table_size

  let dns = Dns.create table_size

  let links = Links.create table_size

  let filter_out (uid : int) (fds : file_descr list) =
    List.filter (fun fd -> fd.uid <> uid) fds

  let remove_socket table (fd : file_descr) =
    match fd.addr with
    | None -> ()
    | Some addr -> (
        match Socket_table.find_opt table addr with
        | None -> ()
        | Some sockets ->
            Socket_table.replace table addr (filter_out fd.uid sockets) )

  let add_socket table (fd : file_descr) =
    match fd.addr with
    | None -> Stdlib.failwith "add_socket: socket has no address"
    | Some addr -> (
        match Socket_table.find_opt table addr with
        | None -> Socket_table.add table addr [fd]
        | Some sockets ->
            if List.exists (fun { uid; _ } -> uid = fd.uid) sockets then
              Stdlib.failwith "add_socket: socket is already in table"
            else Socket_table.replace table addr (filter_out fd.uid sockets) )

  let remove_bound_or_listening bound_table (fd : file_descr) =
    match fd.addr with
    | None -> ()
    | Some addr -> Socket_table.remove bound_table addr

  let add_bound_or_listening bound_table (fd : file_descr) =
    match fd.addr with
    | None -> Stdlib.failwith "add_bound_or_listening: socket has no address"
    | Some addr ->
        if Socket_table.mem bound_table addr then ()
        else Socket_table.add bound_table addr fd

  (* File descriptors have globally unique uids, generated using
     the following functions. *)
  let gen =
    let x = ref 0 in
    fun () ->
      let v = !x in
      incr x ;
      v

  module Make (X : Local_parameters) : Sig.S with type file_descr = file_descr =
  struct
    type nonrec file_descr = file_descr

    let debug = Log.debug X.hostname

    let () =
      let host_entry =
        { Unix.h_name = X.hostname;
          h_aliases = [||];
          h_addrtype = Unix.PF_INET;
          h_addr_list = [| X.address |]
        }
      in
      Dns.add dns X.hostname host_entry

    (* ------------------------------------------------------------------------ *)
    (* Auxiliary functions *)

    (* when writing to an unbound socket, the OS will automatically
       pick a free port. *)
    let assign_port (fd : file_descr) =
      match fd.addr with
      | Some _ ->
          Lwt.fail_with "Local.assign_port: socket in inconsistent state"
      | None ->
          let port = gen_port () in
          ( match fd.dom with
          | PF_UNIX ->
              Lwt.fail_with "Local.assign_port: unix domain not handled"
          | PF_INET | PF_INET6 -> Lwt.return (Unix.ADDR_INET (X.address, port))
          )
          >>= fun addr ->
          fd.addr <- Some addr ;
          Lwt.return addr

    (* ------------------------------------------------------------------------ *)
    (* Implementation of Sig.S *)

    let socket (dom : Unix.socket_domain) (kind : Unix.socket_type)
        protocol_type =
      if protocol_type <> 0 then
        Stdlib.failwith "Local.socket: unhandled protocol type"
      else
        let () =
          match dom with
          | Unix.PF_UNIX ->
              Stdlib.failwith "Local.socket: UNIX domain not handled"
          | Unix.PF_INET | Unix.PF_INET6 -> ()
        in
        let uid = gen () in
        let state =
          match kind with
          | Unix.SOCK_RAW | Unix.SOCK_SEQPACKET ->
              Stdlib.failwith
                "Local.socket: SOCK_RAW, SOCK_SEQPACKET types not handled"
          | Unix.SOCK_STREAM ->
              Stream_state
                { stream_state = Stream_neutral;
                  conns_lock = Lwt_mutex.create ();
                  conns_cond = Lwt_condition.create ()
                }
          | Unix.SOCK_DGRAM ->
              Datagram_state
                { datagram_state = Datagram_neutral;
                  datagrams = Sockaddr_map.empty;
                  datagram_lock = Lwt_mutex.create ();
                  datagram_cond = Lwt_condition.create ()
                }
        in
        { uid; dom; kind; addr = None; state }

    let setsockopt (fd : file_descr) (opt : Unix.socket_bool_option)
        (flag : bool) =
      ignore fd ;
      ignore flag ;
      match opt with
      | Unix.SO_REUSEADDR | Unix.SO_KEEPALIVE | Unix.TCP_NODELAY -> ()
      | _ ->
          let s =
            Format.asprintf
              "Local.setsockopt: %a not implemented"
              pp_socket_bool_option
              opt
          in
          Stdlib.failwith s

    let bind_datagram (fd : file_descr) (addr : Unix.sockaddr) =
      match fd.state with
      | Stream_state _ -> assert false
      | Datagram_state state -> (
          match state.datagram_state with
          | Datagram_bound | Datagram_connected ->
              Lwt.fail Unix.(Unix_error (EINVAL, "bind", ""))
          | Datagram_closed -> Lwt.fail Unix.(Unix_error (EBADF, "bind", ""))
          | Datagram_neutral ->
              fd.addr <- Some addr ;
              state.datagram_state <- Datagram_bound ;
              add_bound_or_listening datagram_table fd ;
              debug
                (Format.asprintf "added %a to bound table\n" pp_sockaddr addr) ;
              Lwt.return_unit )

    let bind_stream (fd : file_descr) (addr : Unix.sockaddr) =
      match fd.state with
      | Datagram_state _ -> assert false
      | Stream_state state -> (
          match state.stream_state with
          | Stream_bound | Stream_listening _ | Stream_connecting _
          | Stream_connected _ ->
              Lwt.fail Unix.(Unix_error (EINVAL, "bind", ""))
          | Stream_closed -> Lwt.fail Unix.(Unix_error (EBADF, "bind", ""))
          | Stream_neutral ->
              fd.addr <- Some addr ;
              state.stream_state <- Stream_bound ;
              add_socket stream_table fd ;
              add_bound_or_listening bound_stream_table fd ;
              debug
                (Format.asprintf "added %a to bound table\n" pp_sockaddr addr) ;
              Lwt.return_unit )

    let bind (fd : file_descr) (addr : Unix.sockaddr) =
      match fd.state with
      | Datagram_state _ -> bind_datagram fd addr
      | Stream_state _ -> bind_stream fd addr

    let listen (fd : file_descr) (max_connections : int) =
      match fd.state with
      | Datagram_state _ -> raise Unix.(Unix_error (EINVAL, "listen", ""))
      | Stream_state state -> (
          match state.stream_state with
          | Stream_closed -> raise Unix.(Unix_error (EBADF, "listen", ""))
          | Stream_neutral | Stream_listening _ | Stream_connecting _
          | Stream_connected _ ->
              raise Unix.(Unix_error (EINVAL, "listen", ""))
          | Stream_bound ->
              let max_connections = max 1 max_connections in
              state.stream_state <-
                Stream_listening { max_connections; pending = [] } )

    let create_channels points : channels =
      let read_from = Simple_pipe.create X.buffer_size in
      let write_to = Simple_pipe.create X.buffer_size in
      { points; read_from; write_to }

    let revert { points; read_from; write_to } =
      { points; read_from = write_to; write_to = read_from }

    let perform_accept (fd : file_descr) pending =
      debug "accept: perform_accept" ;
      let client_socket = pending.client_socket in
      match client_socket.state with
      | Datagram_state _ ->
          Lwt.fail_with "accept: datagram client socket (bug found)"
      | Stream_state state -> (
          match state.stream_state with
          | Stream_neutral | Stream_bound | Stream_listening _
          | Stream_connected _ ->
              debug "accept: wrong client socket" ;
              Lwt.fail_with "accept: insonsistent state"
          | Stream_closed ->
              debug "accept: closed client socket" ;
              Lwt.return None
          | Stream_connecting accepted_cond -> (
              let () = debug "accept: socket connecting" in
              ( match client_socket.addr with
              | None ->
                  Lwt.fail_with "Local.accept: distant socket has no address"
              | Some addr -> Lwt.return addr )
              >>= fun client_addr ->
              let () = debug "accept: got remote addr" in
              let comm_socket = socket fd.dom fd.kind 0 in
              (* communication socket inherits address of listening socket *)
              comm_socket.addr <- fd.addr ;
              add_socket stream_table comm_socket ;
              let point1 = comm_socket.uid in
              let point2 = client_socket.uid in
              let points = (point1, point2) in
              let channels = create_channels points in
              match comm_socket.state with
              | Datagram_state _ -> assert false
              | Stream_state state ->
                  state.stream_state <- Stream_connected channels ;
                  Links.add links points () ;
                  Lwt_condition.signal accepted_cond (Some (revert channels)) ;
                  Lwt.return (Some (comm_socket, client_addr)) ) )

    let accept (fd : file_descr) =
      match fd.state with
      | Datagram_state _ ->
          Lwt.fail Unix.(Unix_error (EOPNOTSUPP, "accept", ""))
      | Stream_state state -> (
          match state.stream_state with
          | Stream_closed -> Lwt.fail Unix.(Unix_error (EBADF, "accept", ""))
          | Stream_neutral | Stream_connected _ | Stream_connecting _
          | Stream_bound ->
              Lwt.fail Unix.(Unix_error (EINVAL, "accept", ""))
          | Stream_listening listening_state ->
              let rec until_success () =
                Lwt_mutex.lock state.conns_lock >>= fun () ->
                ( match listening_state.pending with
                | [] -> (
                    Lwt_condition.wait ~mutex:state.conns_lock state.conns_cond
                    >>= fun () ->
                    match listening_state.pending with
                    | [] -> Lwt.fail_with "accept: empty pending queue"
                    | conn :: rest ->
                        listening_state.pending <- rest ;
                        perform_accept fd conn )
                | conn :: rest ->
                    listening_state.pending <- rest ;
                    perform_accept fd conn )
                >>= fun res ->
                Lwt_mutex.unlock state.conns_lock ;
                match res with
                | None -> Lwt_main.yield () >>= fun () -> until_success ()
                | Some result ->
                    let () =
                      debug
                        (Format.asprintf
                           "accept: success (%a, %a)"
                           pp_file_descr
                           (fst result)
                           Helpers.pp_sockaddr
                           (snd result))
                    in
                    Lwt.return result
              in
              until_success () )

    let perform_connect_stream fd addr =
      match Socket_table.find_opt bound_stream_table addr with
      | None ->
          Lwt.fail
            Unix.(Unix_error (EHOSTUNREACH, Format.asprintf "connect", ""))
      | Some socket -> (
          match socket.state with
          | Datagram_state _ ->
              Lwt.fail
                Unix.(Unix_error (ECONNREFUSED, Format.asprintf "connect", ""))
          | Stream_state state -> (
              match state.stream_state with
              | Stream_neutral | Stream_connecting _ | Stream_connected _
              | Stream_closed | Stream_bound ->
                  Lwt.fail_with
                    "Local.connect: distant socket not in listening state"
              | Stream_listening listening_state -> (
                  let () = debug "perform_connect: entered" in
                  let num_pending = List.length listening_state.pending in
                  if listening_state.max_connections <= num_pending then
                    Lwt.fail
                      Unix.(
                        Unix_error (ECONNREFUSED, Format.asprintf "connect", ""))
                  else
                    let accepted_cond = Lwt_condition.create () in
                    let fd_state =
                      match fd.state with
                      | Datagram_state _ -> assert false
                      | Stream_state fd_state -> fd_state
                    in
                    fd_state.stream_state <- Stream_connecting accepted_cond ;
                    let new_pending = { client_socket = fd } in
                    listening_state.pending <-
                      new_pending :: listening_state.pending ;
                    Lwt_condition.signal state.conns_cond () ;
                    let () =
                      debug
                        (Format.asprintf
                           "perform_connect: waiting accept by %d"
                           socket.uid)
                    in
                    Lwt_condition.wait accepted_cond >>= function
                    | None -> (
                        (* Connection failed. *)
                        (* If connect() fails, consider the state of the socket as unspecified.
                           Portable applications should close the socket and create a new one
                           for reconnecting. *)
                        debug "perform_connect: failed" ;
                        match fd_state.stream_state with
                        | Stream_closed ->
                            (* fd was closed while waiting (client-side failure) *)
                            Lwt.return_unit
                        | Stream_connecting _ ->
                            (* server refused our connection *)
                            Lwt.fail
                              Unix.(Unix_error (ECONNREFUSED, "connect", ""))
                        | _ -> assert false )
                    | Some channels ->
                        debug "perform_connect: success" ;
                        fd_state.stream_state <- Stream_connected channels ;
                        Lwt.return_unit ) ) )

    let connect_datagram _fd _addr = assert false (* TODO *)

    let connect_stream (fd : file_descr) (addr : Unix.sockaddr) =
      match fd.state with
      | Datagram_state _ -> assert false
      | Stream_state state -> (
          match state.stream_state with
          | Stream_closed -> Lwt.fail Unix.(Unix_error (EBADF, "connect", ""))
          | Stream_listening _ | Stream_connecting _ | Stream_connected _ ->
              Lwt.fail_with
                "Local.connect: invalid state (listening or already connected)"
          | Stream_neutral -> (
              match fd.addr with
              | Some _ ->
                  Lwt.fail_with "Local.connect: socket in inconsistent state"
              | None ->
                  assign_port fd >>= fun _fresh_addr ->
                  perform_connect_stream fd addr )
          | Stream_bound -> perform_connect_stream fd addr )

    let connect (fd : file_descr) (addr : Unix.sockaddr) =
      match fd.state with
      | Datagram_state _ -> connect_datagram fd addr
      | Stream_state _ -> connect_stream fd addr

    let read (fd : file_descr) (bytes : Bytes.t) (ofs : int) (len : int) =
      let () = debug (Format.asprintf "read: %d bytes" len) in
      match fd.state with
      | Datagram_state _ ->
          Lwt.fail_with "Local.read: SOCK_DGRAM sockets not handled yet"
      | Stream_state fd_state -> (
          match fd_state.stream_state with
          | Stream_closed -> Lwt.fail Unix.(Unix_error (EBADF, "read", ""))
          | Stream_neutral | Stream_listening _ | Stream_connecting _
          | Stream_bound ->
              Lwt.fail Unix.(Unix_error (ENOTCONN, "read", ""))
          | Stream_connected { read_from; _ } ->
              let () =
                debug (Format.asprintf "read (%d): accessing pipe..." fd.uid)
              in
              Simple_pipe.read read_from bytes ofs len )

    let write (fd : file_descr) (bytes : Bytes.t) (ofs : int) (len : int) =
      let () = debug (Format.asprintf "write: %d bytes" len) in
      match fd.state with
      | Datagram_state _ ->
          Lwt.fail_with "Local.read: SOCK_DGRAM sockets not handled yet"
      | Stream_state fd_state -> (
          match fd_state.stream_state with
          | Stream_closed ->
              Lwt.fail Unix.(Unix_error (EBADF, "Local.write", ""))
          | Stream_neutral | Stream_listening _ | Stream_connecting _
          | Stream_bound ->
              Lwt.fail Unix.(Unix_error (ENOTCONN, "write", ""))
          | Stream_connected { write_to; _ } ->
              let () = debug "write: accessing pipe..." in
              Simple_pipe.write write_to bytes ofs len )

    let perform_recvfrom dgram_sock bytes ofs len flags =
      ignore flags ;
      let rec try_to_recv () =
        match Sockaddr_map.choose_opt dgram_sock.datagrams with
        | Some (addr, queue) -> (
            match Queue.take_opt queue with
            | Some msg ->
                Bytes.blit msg 0 bytes ofs len ;
                Lwt.return (Bytes.length msg, addr)
            | None -> wait_message () )
        | None -> wait_message ()
      and wait_message () =
        Lwt_mutex.with_lock dgram_sock.datagram_lock (fun () ->
            Lwt_condition.wait
              ~mutex:dgram_sock.datagram_lock
              dgram_sock.datagram_cond
            >>= fun () -> try_to_recv ())
      in
      try_to_recv ()

    let recvfrom (fd : file_descr) (bytes : Bytes.t) (ofs : int) (len : int)
        (flags : Unix.msg_flag list) =
      match fd.state with
      | Stream_state _ -> Lwt.fail Unix.(Unix_error (EINVAL, "recvfrom", ""))
      | Datagram_state fd_state -> (
          match fd_state.datagram_state with
          | Datagram_connected | Datagram_closed ->
              Lwt.fail Unix.(Unix_error (EBADF, "recvfrom", ""))
          | Datagram_neutral | Datagram_bound ->
              perform_recvfrom fd_state bytes ofs len flags )

    let perform_sendto ~from ~to_ fd bytes ofs len flags =
      ignore flags ;
      ignore fd ;
      match Socket_table.find_opt datagram_table to_ with
      | None -> Lwt.fail Unix.(Unix_error (EHOSTUNREACH, "sendto", ""))
      | Some target_socket -> (
          add_bound_or_listening datagram_table fd ;
          (* add_if_absent fd >>= fun () -> *)
          (* /!!!!!!!\ we need to allow the server to send back to the client *)
          match target_socket.state with
          | Stream_state _ ->
              Lwt.fail_with "Local.perform_sendto: inconsistent state"
          | Datagram_state state ->
              let map = state.datagrams in
              let pipe =
                match Sockaddr_map.find_opt from map with
                | None ->
                    let pipe = Queue.create () in
                    let map = Sockaddr_map.add from pipe map in
                    state.datagrams <- map ;
                    pipe
                | Some pipe -> pipe
              in
              let msg = Bytes.sub bytes ofs len in
              Queue.add msg pipe ;
              Lwt_condition.signal state.datagram_cond () ;
              Lwt.return (Bytes.length msg) )

    let sendto (fd : file_descr) (bytes : Bytes.t) (ofs : int) (len : int)
        (flags : Unix.msg_flag list) (addr : Unix.sockaddr) =
      match fd.state with
      | Stream_state _ ->
          Lwt.fail_with "Local.sendto: can only be used in SOCK_DGRAM mode"
      | Datagram_state fd_state -> (
          match fd_state.datagram_state with
          | Datagram_connected ->
              (* TODO: store address given in prev call to connect in constructor.
                 check that [addr] matches, ECONNREFUSED otherwise. *)
              Lwt.fail_with
                "Local.sendto: connected datagram sockets not handled yet"
          | Datagram_closed -> Lwt.fail Unix.(Unix_error (EBADF, "sendto", ""))
          | Datagram_neutral | Datagram_bound -> (
              match (fd.addr, fd_state.datagram_state) with
              | (None, Datagram_bound) ->
                  Lwt.fail_with "Local.sendto: inconsistent state"
              | (None, Datagram_neutral) ->
                  assign_port fd >>= fun from ->
                  perform_sendto ~from ~to_:addr fd bytes ofs len flags
              | (None, _) -> assert false
              | (Some from, _) ->
                  perform_sendto ~from ~to_:addr fd bytes ofs len flags ) )

    let sleep = Time_impl.sleep

    let now = Time_impl.now

    let close (fd : file_descr) =
      let () = debug (Format.asprintf "closing %a" pp_file_descr fd) in
      match fd.state with
      | Datagram_state state -> (
          match state.datagram_state with
          | Datagram_closed -> Lwt.fail Unix.(Unix_error (EBADF, "close", ""))
          | Datagram_neutral | Datagram_bound | Datagram_connected ->
              remove_bound_or_listening datagram_table fd ;
              state.datagram_state <- Datagram_closed ;
              Lwt.return () )
      | Stream_state state -> (
          match state.stream_state with
          | Stream_closed -> Lwt.fail Unix.(Unix_error (EBADF, "close", ""))
          | Stream_neutral ->
              (* TODO in debug mode, assert it's not in any table *)
              state.stream_state <- Stream_closed ;
              Lwt.return ()
          | Stream_listening { pending; _ } ->
              (* Iterate on pending connections, signaling that their connection
                 attempt failed. *)
              List.iter
                (fun { client_socket } ->
                  match client_socket.state with
                  | Datagram_state _ ->
                      Stdlib.failwith
                        "close: pending connection has inconsistent state"
                  | Stream_state
                      { stream_state = Stream_connecting accepted_cond; _ } ->
                      Lwt_condition.signal accepted_cond None
                  | Stream_state { stream_state = Stream_closed; _ } -> ()
                  | _ -> assert false)
                pending ;
              remove_bound_or_listening bound_stream_table fd ;
              remove_socket stream_table fd ;
              Lwt.return ()
          | Stream_connecting accepted_cond ->
              (* [fd] is being closed while waiting for an accept. *)
              state.stream_state <- Stream_closed ;
              Lwt_condition.signal accepted_cond None ;
              Lwt.return ()
          | Stream_connected { read_from; write_to; points } ->
              state.stream_state <- Stream_closed ;
              (* TODO: in debug mode, assert it is not in the bound table *)
              remove_socket stream_table fd ;
              Links.remove links points ;
              Simple_pipe.writer_close write_to >>= fun () ->
              Simple_pipe.reader_close read_from
          | Stream_bound -> (
              match fd.addr with
              | None -> Lwt.fail_with "Local.close: inconsistent state"
              | Some _addr ->
                  remove_bound_or_listening bound_stream_table fd ;
                  remove_socket stream_table fd ;
                  state.stream_state <- Stream_closed ;
                  Lwt.return_unit ) )

    let gethostname () = Lwt.return X.hostname

    let gethostbyname hostname =
      let result = Dns.find dns hostname in
      Lwt.return result
  end

  let make ~hostname ~address ~buffer_size =
    let module L = Make (struct
      let hostname = hostname

      let address = address

      let buffer_size = buffer_size
    end) in
    (module L : Sig.S with type file_descr = file_descr)
end

module Init () = Init_common (struct
  let sleep = Lwt_unix.sleep

  let now = Unix.gettimeofday
end)

module Init_with_faked_time () = struct
  module Sleep_scheduler = Sleep_queue.Create ()

  let elapse = Sleep_scheduler.elapse

  let sleep = Sleep_scheduler.sleep

  let now = Sleep_scheduler.now

  include Init_common (struct
    let sleep = Sleep_scheduler.sleep

    let now = Sleep_scheduler.now
  end)
end
