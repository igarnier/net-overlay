let asprintf = Format.asprintf

(* let debug hostname msg = Format.eprintf "%s: %s@." hostname msg *)

let debug hostname msg =
  ignore hostname ;
  ignore msg
