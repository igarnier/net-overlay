type file_descr

module type Local_parameters = sig
  val hostname : string

  val address : Unix.inet_addr

  val buffer_size : int
end

module Init () : sig
  module Make (X : Local_parameters) : Sig.S with type file_descr = file_descr

  val make :
    hostname:string ->
    address:Unix.inet_addr ->
    buffer_size:int ->
    file_descr Sig.t
end

module Init_with_faked_time () : sig
  (** [elapse n] makes [n] simulated seconds elapse. *)
  val elapse : float -> unit

  (** [sleep n] makes the calling thread block [n] simulated seconds. *)
  val sleep : float -> unit Lwt.t

  (** [now ()] returns the number of seconds elapsed since the faked [epoch]. *)
  val now : unit -> float

  module Make (X : Local_parameters) : Sig.S with type file_descr = file_descr

  val make :
    hostname:string ->
    address:Unix.inet_addr ->
    buffer_size:int ->
    file_descr Sig.t
end
